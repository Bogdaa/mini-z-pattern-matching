const babel = require('@babel/parser');
const traverse = require('@babel/traverse').default;
const deepEqual = require('deep-equal');
const areEqualByValueOrType = require('./helpers/areEqualByValueOrType');
const { parseToAst, createObjectFromAST } = require('./helpers/ast');


const matches = (x) => (...callbacks) => {
    return [...callbacks]
        .map(cb => {
            const ast = babel.parse(cb.toString())
            let isEqual = true;
            traverse(ast,{
                ArrowFunctionExpression({ node }) {
                    node.params.forEach((node,idx) => {
                        if(node.right) {
                            if(node.right.type === 'NullLiteral') {
                                isEqual = x === null
                                return;
                            }

                            if(node.right.type === 'ObjectExpression') {
                                isEqual = deepEqual(x, createObjectFromAST(node.right.properties))
                                return;
                            }

                            if(node.right.type === 'ArrayExpression') {
                                isEqual = deepEqual(x, parseToAst(node.right))
                                return;
                            }

                            if(node.right.name) {
                                isEqual = areEqualByValueOrType(x,node.right.name)
                                return;
                            }

                            isEqual = areEqualByValueOrType(x,node.right.value)
                        }
                    })
                }
            })
            return { value:cb(x), isEqual }
        })
        .filter(({ isEqual }) => isEqual)
        .map(({ value }) => value)
}


console.log(matches([{ foo:'bar'},{}, null])(
    (x= [{foo: 'bar'},{},null]) => `Value is`,
    (x = String) => 'Value is 1'
))