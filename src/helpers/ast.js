const parseToAst = (ast) => {
    switch (ast.type) {
        case 'ArrayExpression': {
            return ast.elements.map(parseToAst)
        }
        case 'ObjectExpression': {
            return createObjectFromAST(ast.properties)
        }
        case 'NullLiteral': {
            return null;
        }
        default: {
            return ast.value;
        }
    }
}

const createObjectFromAST = (properties) => {
    return Object.fromEntries(properties.map((property) => [property.key.name,parseToAst(property.value)]))
}

module.exports = {
    parseToAst,
    createObjectFromAST,
}