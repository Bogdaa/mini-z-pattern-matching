const areEqualByValueOrType = (a,b) => {
    if(typeof(b) === 'string' &&
        ["number","undefined","boolean","string","object"].includes(b.toLowerCase())) {
        return typeof(a) === b.toLowerCase()
    }

    return a === b;
}

module.exports = areEqualByValueOrType;